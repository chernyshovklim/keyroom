package space.chernyshov.keyroom;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.security.keystore.StrongBoxUnavailableException;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 1;

    TextView ChatBox;
    ImageButton OnOffBtn, mImBtnBlue, InfoOutputBtn;
    BluetoothAdapter BlueAdapter;
    ScrollView ChatBoxView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // disable title bar - start
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // disable title bar - end

        // initialize values - start
        ChatBox = findViewById(R.id.text_view_in_sb);
        OnOffBtn = findViewById(R.id.bluetooth_button);
        InfoOutputBtn = findViewById(R.id.bluetooth_button_paired);
        BlueAdapter = BluetoothAdapter.getDefaultAdapter();
        ChatBoxView = findViewById(R.id.masage_liner);
        // initialize values - end

        // OnOffBtn click - start
        OnOffBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!BlueAdapter.isEnabled()) {
                    OnOffBtn.setImageResource(R.drawable.ic_baseline_bluetooth_24);
                    //intent to on bluetooth
                    Intent intentActivateBlue = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intentActivateBlue, REQUEST_ENABLE_BT);
                    //intent to discover devices
                    Intent intentDiscoverDevices = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(intentDiscoverDevices, REQUEST_DISCOVER_BT);
                    ChatBox.append("Bluetooth включился \n \n");

                } else if (BlueAdapter.isEnabled()) {
                    OnOffBtn.setImageResource(R.drawable.ic_baseline_bluetooth_disabled_24);
                    BlueAdapter.disable();
                    ChatBox.append("Bluetooth уже выключился \n \n");
                } else {
                    ChatBox.append("Нужно отследить... Не включение, не выключение bluetooth не подошло. \n \n");
                }


            }

        });
        // OnOffBtn click - end

        // Adding button to scrollView - start
        ScrollView scrollview = (ScrollView) findViewById(R.id.masage_liner);
        Set<BluetoothDevice> arr = BlueAdapter.getBondedDevices();
        for (BluetoothDevice device : arr) {
            Button button = new Button(this);
            button.setText(device.getName());
        }
    }
        catch (Exception e) {
        ChatBox.append("Ошибка " + e + " \n \n");
    }
    }
}
